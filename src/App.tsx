import React from "react";
import "./App.css";
import Tracker from "./tracker";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Tracker />
      </header>
    </div>
  );
}

export default App;
