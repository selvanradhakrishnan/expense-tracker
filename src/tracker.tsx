import React from "react";
import "./App.css";
import moment from "moment";

function Tracker() {
  interface ArrayTrance {
    date: Date;
    state: String;
    count: any;
  }
  const [state, setState] = React.useState({
    inputValue: 0,
    transactions: Array<ArrayTrance>(),
    count: 0,
    error: false,
  });
  const handleChangeButton = (event: String) => {
    if (state.inputValue) {
      let transaction = state.transactions;
      let countGet = state.count;
      if (event === "Add") {
        let addCount = Number(countGet) + Number(state.inputValue);
        let arrayTrances = {
          date: new Date(),
          state: event,
          count: state.inputValue,
        };
        transaction.push(arrayTrances);
        setState({ ...state, count: addCount, inputValue: 0, error: false });
      } else {
        let RemoveCount = Number(countGet) - Number(state.inputValue);
        let arrayTrances = {
          date: new Date(),
          state: event,
          count: state.inputValue,
        };
        transaction.push(arrayTrances);
        setState({ ...state, count: RemoveCount, inputValue: 0, error: false });
      }
    } else {
      setState({ ...state, error: true });
    }
  };
  const handleInput = (event: any) => {
    setState({ ...state, inputValue: event.target.value, error: false });
  };
  return (
    <div className="total-container">
      <h4 className="title">Expense Tracker</h4>
      <div className="input-container">
        <div className="balance">Balance:{state.count}</div>
        <div>
          <input
            type="number"
            value={state.inputValue}
            onChange={(e) => handleInput(e)}
            className="input-field"
            placeholder="please enter the value"
          />
          {state.error && <div className="error">Field must be required!</div>}
        </div>
        <button
          type="button"
          onClick={() => handleChangeButton("Add")}
          className="add-button"
        >
          Add
        </button>
        <button
          type="button"
          onClick={() => handleChangeButton("Remove")}
          className="add-button"
        >
          Remove
        </button>
      </div>
      <div className="transaction-container">
        <div>Transaction</div>
        {state.transactions.map((val, index) => {
          return (
            <div className="mappedValue" key={index}>
              {moment(val.date).format("YYYY-MM-DD hh:mm:ss")} - {val.count} -{" "}
              {val.state}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Tracker;
